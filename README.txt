CROSSITE MODULE

This module seems to work pretty well on my development test box. I will be
using it as part of my Drupal 4.7.x -> Drupal 5.x migration soon. When that
is complete I'll consider removing the beta marker on the release.

WHAT DOES IT DO? HOW DOES IT WORK?

You, a Drupal site maintainer, create a multisite installation where the nodes,
vocabularies, and terms are shared. You can then associate a term with one or
more sites. When a visitor visits a node marked with a term belonging to a
different site, the visitor is redirected to see the node on that other site
rather than being shown the node on the current site.

If none of the terms associated with a node belong to a particular site or
group of sites, the node will be shown to the user on any site without a
redirect. The module will not redirect unless the node belongs to terms that
belong to a given set of sites and none of the sites are the site the user is
currently on. If a node belongs to multiple sites and a redirect is going to
be made, it will pick one of the sites without any particular preference.

It doesn't matter if the sites are on different domains or just different paths
on the same domain. It simply performs the redirect.

WHAT DOESN'T IT DO?

It does not perform redirects for anything but nodes. In the future, other
kinds of redirects may be added, but nodes are all it touches for now.

It doesn't care about authentication or access control. If special privileges
are required to access a node, you probably want to look into getting the
Single Sign On module or something similar to make sure a user's session is
shared across the sites. Otherwise, they might need to login multiple times as
they get bounced from site to site.

SIMILAR MODULES

The only similar module I'm currently aware of is the Multidomain module:

http://drupal.org/project/multidomain

That module comes at this problem from a completely different angle and is
more powerful. If you find this module is too weak for your needs, you might
want to look into Multidomain instead.

RECENT CHANGES

This release no longer requires manual configuration in a shared_variables.php
file. However, it does perform some possibly underhanded tricks to make this
work. It shouldn't damage anything (no warranties on that), but it might not
work correctly if you have weird and wacky stuff in your settings.php files.

If you want to use the 4.7 version of this module READ THE INCLUDED
DOCUMENTATION because many things have changed.

GETTING STARTED

Step 1: Multi-Site Configuration

You must first install and run a Drupal multisite installation. The Drupal
handbook features documentation on this subject, so I'm not going to go into
too much detail on that. 

However, you must make sure that at least the following are shared:

 1. The taxonomy tables.
 2. crossite_domains_tid (which is installed by this module)

The rest of your multisite configuration is up to you.

You might be able to get away without sharing the taxonomy tables if you know
what you're doing, but that's not my recommendation.

Step 2: Module Installation

Install the module and activate in the modules screen as is done with your
typical Drupal module.

Step 3: Crossite Configuration

Create or edit terms and use the checkboxes now at the bottom of the term edit
screens to configure which sites a term marks a node as belonging to.

TROUBLESHOOTING

If you have trouble with the redirects going to weird places, you might want to
make sure the $base_url variable is set in all of your settings.php.

If you make a change to your $base_urls or run into other problems when your
$base_url settings seem to be correct, you probably need to clear out the
cache table. You can do this by running this SQL command:

DELETE FROM cache WHERE cid LIKE 'crossite_%';

(You may need to rename "cache" if you are using table prefixes.)

GETTING HELP

If you have any questions or bugs to report, submit issues to:

http://drupal.org/project/crossite 

You can check the issue list there for latest bug information as well.
